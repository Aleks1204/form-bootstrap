<?php

# Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия)
$country = $_POST['country'];
$day = ($_POST['days'] * 400);
$res = $day + (($day / 100) * $country);
$discount = $_POST['discount'];
if ($discount == true) {
    $mony = $res - ($res * 0.05);
} else {
    $mony = $res;
}


?>

<!DOCTYPE html>
<html>
<head>
    <title>form</title>
    <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        body{
            background-color: silver;
            background-image: linear-gradient(335deg, #b00 23px, transparent 23px),
            linear-gradient(155deg, #d00 23px, transparent 23px),
            linear-gradient(335deg, #b00 23px, transparent 23px),
            linear-gradient(155deg, #d00 23px, transparent 23px);
            background-size: 58px 58px;
            background-position: 0px 2px, 4px 35px, 29px 31px, 34px 6px;
        }
        .form {
            background-image:linear-gradient( #afc1c9 8%, #384145 100%);
            margin:70px auto;
            width:500px;
            height:350px;
            padding: 40px;
            color:white;
            box-shadow:0 8px 15px rgba(0,0,0,.6);
        }

    </style>
</head>
<body>
<!-- выбор и расчет тура -->
<div class="form">
    <h2>Расчитать стоимость тура</h2>
    <form action="" method="POST">
        <select name="country" class="form-select" aria-label="Default select example">
            <option disabled selected>Выберите страну</option>
            <option value="1">Турция</option>
            <option value="10">Египет</option>
            <option value="12">Италия</option>
            </optgroup>
        </select>
        <P>введите количесто дней</P>
        <input type="number" name="days" class="form-control">
        <p>отметьте, если у вас есть скидка
            <input type="checkbox" name="discount" class="form-check-input"></p>
        <input type="submit" value="поcчитать" class="btn btn-primary">
        <p>Приготовтесь расстаться с $<?= $mony; ?></p>
    </form>
</div>
</body>
</html>
